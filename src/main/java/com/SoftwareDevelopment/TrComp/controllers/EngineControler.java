package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Car;
import com.SoftwareDevelopment.TrComp.models.Chip;
import com.SoftwareDevelopment.TrComp.models.Color;
import com.SoftwareDevelopment.TrComp.models.Engine;
import com.SoftwareDevelopment.TrComp.services.ChipService;
import com.SoftwareDevelopment.TrComp.services.EngineService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/engine")
public class EngineControler {
    public EngineControler(EngineService engineService, ChipService chipService) {
        this.chipService = chipService;
        this.engineService = engineService;
    }
    private ChipService chipService;
    private EngineService engineService;

    @GetMapping("/list")
    public String showAllEngine(Pageable page, Model model){

        model.addAttribute("engine", engineService.findAll(page));

        return "engine/show-all-engine.html";
    }

    @GetMapping("/delete")
    public String engineDelete(@RequestParam("id") int id) {
        engineService.deleteById(id);

        return "redirect:/engine/list";
    }

    @GetMapping("/add")
    public String engineAdd(Model model) {

        Engine engine = new Engine();

        model.addAttribute("engine", engine);

        return "engine/update-form";
    }

    @GetMapping("/update")
    public String engineUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("engine", engineService.findById(id));

        return "engine/update-form";
    }

    @GetMapping("/addChip")
    public String addChipEngine (@RequestParam("id_en") Integer id_en, @RequestParam("id_ch") Integer id_ch){
        Engine engine = engineService.findById(id_en);
        Chip chip = chipService.findById(id_ch);
        engine.getChips().add(chip);
        chip.getEngines().add(engine);
        engineService.save(engine);
        chipService.save(chip);
        return "redirect:/engine/list";
    }

    @GetMapping("/chip")
    public String chipList(@RequestParam("id_en") Integer id_en, Model model){

        model.addAttribute("chip", chipService.findAll());

        model.addAttribute("engine", engineService.findById(id_en));

        return  "engine/all-chip";
    }

    @GetMapping("/selectChip")
    public String selectChipEngine(@RequestParam("id_en") Integer id_en, Model model){

        Engine engine = engineService.findById(id_en);

        model.addAttribute("chip", chipService.findAll());

        model.addAttribute("engine", engineService.findById(id_en));

        return "engine/add-chip-to-engine";
    }

    @PostMapping("/save")
    public String engineSave(@ModelAttribute("engine") Engine engine) {

        engineService.save(engine);

        return "redirect:/engine/list";
    }
}
