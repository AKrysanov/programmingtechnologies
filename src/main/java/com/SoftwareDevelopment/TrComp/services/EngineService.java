package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Engine;
import com.SoftwareDevelopment.TrComp.repositories.EngineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EngineService {
    @Autowired
    EngineRepository engineRepository;
    public Engine findById(Integer id) {
        Optional<Engine> result = engineRepository.findById(id);
        Engine n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Engine> findAll() {
        return engineRepository.findAll();
    }

    public Iterable<Engine> findAll(Pageable pageable) {
        return engineRepository.findAll(pageable);
    }

    public Iterable<Engine> findAll(Sort sort) {
        return engineRepository.findAll(sort);

    }

    public void save(Engine engine) {
        engineRepository.save(engine);
    }

    public void deleteById(Integer id){
        engineRepository.deleteById(id);
    }
}
