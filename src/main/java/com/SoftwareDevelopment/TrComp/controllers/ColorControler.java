package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.exportes.*;
import com.SoftwareDevelopment.TrComp.models.Color;
import com.SoftwareDevelopment.TrComp.services.CarService;
import com.SoftwareDevelopment.TrComp.services.ColorService;
import com.lowagie.text.DocumentException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/color")
public class ColorControler {
    public ColorControler(CarService carService, ColorService colorService) {
        this.carService = carService;
        this.colorService = colorService;

    }
    private CarService carService;
    private ColorService colorService;

    @GetMapping("/list")
    public String showAllColor(Pageable page, Model model){

        model.addAttribute("color", colorService.findAll(page));

        return "color/show-all-color.html";
    }

    @GetMapping("/delete")
    public String colorDelete(@RequestParam("id") int id) {
        colorService.deleteById(id);

        return "redirect:/color/list";
    }

    @GetMapping("/add")
    public String colorAdd(Model model) {

        Color color = new Color();

        model.addAttribute("color", color);

        return "color/update-form";
    }

    @GetMapping("/update")
    public String colorUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("color", colorService.findById(id));

        return "color/update-form";
    }

    @PostMapping("/save")
    public String customerSave(@ModelAttribute("color") Color color) {
        colorService.save(color);
        return "redirect:/color/list";
    }

    @GetMapping("/exportToXLSX")
    public void exportToExcel(HttpServletResponse response) throws IOException {

        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("UTF-8");

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=colors.xlsx";

        response.setHeader(headerKey, headerValue);

        java.util.List<Color> colorList = (java.util.List<Color>)colorService.findAll();

        ColorExcelExporter exporter = new ColorExcelExporter(colorList);

        exporter.export(response);
    }

    @GetMapping("/exportToPDF")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        response.setCharacterEncoding("UTF-8");

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=colors.pdf";

        response.setHeader(headerKey, headerValue);

        java.util.List<Color> colorList = (List<Color>)colorService.findAll();

        ColorPdfExporter exporter = new ColorPdfExporter(colorList);

        exporter.export(response);

    }
}
