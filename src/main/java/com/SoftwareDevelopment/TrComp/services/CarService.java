package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Car;
import com.SoftwareDevelopment.TrComp.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CarService {
    @Autowired
    CarRepository carRepository;
    public Car findById(Integer id) {
        Optional<Car> result = carRepository.findById(id);
        Car n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Car> findAll() {
        return carRepository.findAll();
    }

    public Iterable<Car> findAll(Pageable pageable) {
        return carRepository.findAll(pageable);
    }

    public Iterable<Car> findAll(Sort sort) {
        return carRepository.findAll(sort);

    }

    public void save(Car Car) {
        carRepository.save(Car);
    }

    public void deleteById(Integer id){
        carRepository.deleteById(id);
    }
}
