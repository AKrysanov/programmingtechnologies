package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Chip;
import com.SoftwareDevelopment.TrComp.models.Engine;
import com.SoftwareDevelopment.TrComp.services.ChipService;
import com.SoftwareDevelopment.TrComp.services.EngineService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/chip")
public class ChipControler {
    public ChipControler(ChipService chipService, EngineService engineService) {
        this.engineService = engineService;
        this.chipService = chipService;
    }
    private EngineService engineService;
    private ChipService chipService;

    @GetMapping("/list")
    public String showAllChip(Pageable page, Model model){

        model.addAttribute("chip", chipService.findAll(page));

        return "chip/show-all-chip.html";
    }

    @GetMapping("/delete")
    public String chipDelete(@RequestParam("id") int id) {
        chipService.deleteById(id);

        return "redirect:/chip/list";
    }

    @GetMapping("/add")
    public String chipAdd(Model model) {

        Chip chip = new Chip();

        model.addAttribute("chip", chip);

        return "chip/update-form";
    }

    @GetMapping("/update")
    public String chipUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("chip", chipService.findById(id));

        return "chip/update-form";
    }

    @PostMapping("/save")
    public String chipSave(@ModelAttribute("chip") Chip chip) {
        chipService.save(chip);
        return "redirect:/chip/list";
    }

    @GetMapping("/addEngine")
    public String addEngineChip (@RequestParam("id_ch") Integer id_ch, @RequestParam("id_en") Integer id_en){

        Chip chip = chipService.findById(id_ch);
        Engine engine = engineService.findById(id_en);

        chip.getEngines().add(engine);
        engine.getChips().add(chip);

        engineService.save(engine);
        chipService.save(chip);

        return "redirect:/chip/list";
    }

    @GetMapping("/engine")
    public String engineList(@RequestParam("id_ch") Integer id_ch, Model model){

        model.addAttribute("engine", engineService.findAll());

        model.addAttribute("chip", chipService.findById(id_ch));

        return  "chip/all-engine";
    }

    @GetMapping("/selectEngine")
    public String selectEngineChip(@RequestParam("id_ch") Integer id_ch, Model model){

        Chip chip = chipService.findById(id_ch);

        model.addAttribute("engine", engineService.findAll());

        model.addAttribute("chip", chipService.findById(id_ch));

        return "chip/add-engine-to-chip";
    }
}
