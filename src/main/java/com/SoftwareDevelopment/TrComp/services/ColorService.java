package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Color;
import com.SoftwareDevelopment.TrComp.repositories.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ColorService {
    @Autowired
    ColorRepository colorRepository;
    public Color findById(Integer id) {
        Optional<Color> result = colorRepository.findById(id);
        Color n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Color> findAll() {
        return colorRepository.findAll();
    }

    public Iterable<Color> findAll(Pageable pageable) {
        return colorRepository.findAll(pageable);
    }

    public Iterable<Color> findAll(Sort sort) {
        return colorRepository.findAll(sort);

    }

    public void save(Color color) {
        colorRepository.save(color);
    }

    public void deleteById(Integer id){
        colorRepository.deleteById(id);
    }
}
