package com.SoftwareDevelopment.TrComp.controllers;

import com.SoftwareDevelopment.TrComp.models.Car;
import com.SoftwareDevelopment.TrComp.models.Color;
import com.SoftwareDevelopment.TrComp.services.CarService;
import com.SoftwareDevelopment.TrComp.services.ColorService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/car")
public class CarControler {
    public CarControler(ColorService colorService, CarService carService) {
        this.carService = carService;
        this.colorService = colorService;

    }
    private CarService carService;
    private ColorService colorService;


    @GetMapping("/list")
    public String showAllCar(Pageable page, Model model){

        model.addAttribute("car", carService.findAll(page));

        return "car/show-all-car.html";
    }

    @GetMapping("/delete")
    public String carDelete(@RequestParam("id") int id) {
        carService.deleteById(id);

        return "redirect:/car/list";
    }

    @GetMapping("/add")
    public String carAdd(Model model) {

        Car car = new Car();

        model.addAttribute("car", car);

        return "car/update-form";
    }

    @GetMapping("/update")
    public String carUpdate(@RequestParam("id") Integer id, Model model) {

        model.addAttribute("car", carService.findById(id));

        return "car/update-form";
    }

    @PostMapping("/save")
    public String carSave(@ModelAttribute("car") Car car) {
        carService.save(car);
        return "redirect:/car/list";
    }

    @GetMapping("/addColor")
    public String addColorCar (@RequestParam("id") Integer id_bi, @RequestParam("id_col") Integer id_co){
        Car car = carService.findById(id_bi);
        Color color = colorService.findById(id_co);
        car.setColor(color);
        color.getCars().add(car);
        carService.save(car);
        colorService.save(color);
        return "redirect:/car/list";
    }

    @GetMapping("/color")
    public String colorList(@RequestParam("id") Integer id_bi, Model model){

        model.addAttribute("color", colorService.findAll());

        model.addAttribute("car", carService.findById(id_bi));

        return  "car/all-color";
    }
}
