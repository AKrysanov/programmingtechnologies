package com.SoftwareDevelopment.TrComp.services;

import com.SoftwareDevelopment.TrComp.models.Chip;
import com.SoftwareDevelopment.TrComp.repositories.ChipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChipService {
    @Autowired
    ChipRepository chipRepository;
    public Chip findById(Integer id) {
        Optional<Chip> result = chipRepository.findById(id);
        Chip n = null;
        if (result.isPresent()) {
            n = result.get();
        } else {
            throw new RuntimeException("Didn't find");
        }
        return n;
    }

    public Iterable<Chip> findAll() {
        return chipRepository.findAll();
    }

    public Iterable<Chip> findAll(Pageable pageable) {
        return chipRepository.findAll(pageable);
    }

    public Iterable<Chip> findAll(Sort sort) {
        return chipRepository.findAll(sort);

    }

    public void save(Chip chip) {
        chipRepository.save(chip);
    }

    public void deleteById(Integer id){
        chipRepository.deleteById(id);
    }
}
