package com.SoftwareDevelopment.TrComp.exportes;

import com.SoftwareDevelopment.TrComp.models.Color;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.util.List;

public class ColorPdfExporter {

    private List<Color> colorList;

    public ColorPdfExporter(List<Color> colorList) {
        this.colorList = colorList;
    }

    private void fillTableHeader(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(java.awt.Color.WHITE);
        cell.setPadding(4);
        cell.setHorizontalAlignment(Cell.ALIGN_LEFT);

        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setColor(java.awt.Color.WHITE);

        cell.setPhrase(new Phrase("ID"));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Название"));
        table.addCell(cell);
    }

    private void fillTableData(PdfPTable table) {
        for (Color color : colorList) {
            table.addCell(color.getId().toString());
            table.addCell(color.getName());
        }
    }

    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A3);

        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        PdfPTable table = new PdfPTable(2);

        table.setWidthPercentage(100f);
        table.setWidths(new float[] { 1f, 3f});
        table.setSpacingBefore(10);

        fillTableHeader(table);
        fillTableData(table);

        document.add(table);

        document.close();

    }



}
