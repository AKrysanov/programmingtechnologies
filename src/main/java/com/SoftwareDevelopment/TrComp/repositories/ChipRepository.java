package com.SoftwareDevelopment.TrComp.repositories;


import com.SoftwareDevelopment.TrComp.models.Chip;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChipRepository extends JpaRepository<Chip, Integer> {
}
